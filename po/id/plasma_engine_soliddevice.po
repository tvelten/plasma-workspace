# Indonesian translations for plasma_engine_soliddevice package.
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the plasma_engine_soliddevice package.
# Andhika Padmawan <andhika.padmawan@gmail.com>, 2009-2014.
# Wantoyo <wantoyek@gmail.com>, 2017, 2018, 2019, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_engine_soliddevice\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-29 01:56+0000\n"
"PO-Revision-Date: 2022-08-26 23:40+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: https://t.me/Localizations_KDE_Indonesia\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: soliddeviceengine.cpp:112
msgid "Parent UDI"
msgstr "UDI Induk"

#: soliddeviceengine.cpp:113
msgid "Vendor"
msgstr "Vendor"

#: soliddeviceengine.cpp:114
msgid "Product"
msgstr "Produk"

#: soliddeviceengine.cpp:115
msgid "Description"
msgstr "Deskripsi"

#: soliddeviceengine.cpp:116
msgid "Icon"
msgstr "Ikon"

#: soliddeviceengine.cpp:117 soliddeviceengine.cpp:592
msgid "Emblems"
msgstr "Emblem"

#: soliddeviceengine.cpp:118 soliddeviceengine.cpp:474
#: soliddeviceengine.cpp:480 soliddeviceengine.cpp:493
msgid "State"
msgstr "Kondisi"

#: soliddeviceengine.cpp:119 soliddeviceengine.cpp:475
#: soliddeviceengine.cpp:481 soliddeviceengine.cpp:489
#: soliddeviceengine.cpp:491
msgid "Operation result"
msgstr "Hasil operasi"

#: soliddeviceengine.cpp:120
msgid "Timestamp"
msgstr "Cap Waktu"

#: soliddeviceengine.cpp:128
msgid "Processor"
msgstr "Prosesor"

#: soliddeviceengine.cpp:129
msgid "Number"
msgstr "Nomor"

#: soliddeviceengine.cpp:130
msgid "Max Speed"
msgstr "Kecepatan Maksimum"

#: soliddeviceengine.cpp:131
msgid "Can Change Frequency"
msgstr "Dapat Ubah Frekuensi"

#: soliddeviceengine.cpp:139
msgid "Block"
msgstr "Blok"

#: soliddeviceengine.cpp:140
msgid "Major"
msgstr "Mayor"

#: soliddeviceengine.cpp:141
msgid "Minor"
msgstr "Minor"

#: soliddeviceengine.cpp:142
msgid "Device"
msgstr "Peranti"

#: soliddeviceengine.cpp:150
msgid "Storage Access"
msgstr "Akses Penyimpanan"

#: soliddeviceengine.cpp:151 soliddeviceengine.cpp:505
#: soliddeviceengine.cpp:606
msgid "Accessible"
msgstr "Dapat Diakses"

#: soliddeviceengine.cpp:152 soliddeviceengine.cpp:506
msgid "File Path"
msgstr "Alur File"

#: soliddeviceengine.cpp:167
msgid "Storage Drive"
msgstr "Penggerak Penyimpanan"

#: soliddeviceengine.cpp:170
msgid "Ide"
msgstr "Ide"

#: soliddeviceengine.cpp:170
msgid "Usb"
msgstr "Usb"

#: soliddeviceengine.cpp:170
msgid "Ieee1394"
msgstr "Ieee1394"

#: soliddeviceengine.cpp:171
msgid "Scsi"
msgstr "Scsi"

#: soliddeviceengine.cpp:171
msgid "Sata"
msgstr "Sata"

#: soliddeviceengine.cpp:171
msgid "Platform"
msgstr "Platform"

#: soliddeviceengine.cpp:173
msgid "Hard Disk"
msgstr "Hard Disk"

#: soliddeviceengine.cpp:173
msgid "Cdrom Drive"
msgstr "Penggerak Cdrom"

#: soliddeviceengine.cpp:173
msgid "Floppy"
msgstr "Floppy"

#: soliddeviceengine.cpp:174
msgid "Tape"
msgstr "Tape"

#: soliddeviceengine.cpp:174
msgid "Compact Flash"
msgstr "Compact Flash"

#: soliddeviceengine.cpp:174
msgid "Memory Stick"
msgstr "Memory Stick"

#: soliddeviceengine.cpp:175
msgid "Smart Media"
msgstr "Smart Media"

#: soliddeviceengine.cpp:175
msgid "SdMmc"
msgstr "SdMmc"

#: soliddeviceengine.cpp:175
msgid "Xd"
msgstr "Xd"

#: soliddeviceengine.cpp:177
msgid "Bus"
msgstr "Bus"

#: soliddeviceengine.cpp:178
msgid "Drive Type"
msgstr "Tipe Penggerak"

#: soliddeviceengine.cpp:179 soliddeviceengine.cpp:192
#: soliddeviceengine.cpp:360 soliddeviceengine.cpp:374
msgid "Removable"
msgstr "Dapat Dilepas"

#: soliddeviceengine.cpp:180 soliddeviceengine.cpp:193
#: soliddeviceengine.cpp:361 soliddeviceengine.cpp:375
msgid "Hotpluggable"
msgstr "Dapat Dicabut-colok"

#: soliddeviceengine.cpp:202
msgid "Optical Drive"
msgstr "Penggerak Optik"

#: soliddeviceengine.cpp:207
msgid "CD-R"
msgstr "CD-R"

#: soliddeviceengine.cpp:210
msgid "CD-RW"
msgstr "CD-RW"

#: soliddeviceengine.cpp:213
msgid "DVD"
msgstr "DVD"

#: soliddeviceengine.cpp:216
msgid "DVD-R"
msgstr "DVD-R"

#: soliddeviceengine.cpp:219
msgid "DVD-RW"
msgstr "DVD-RW"

#: soliddeviceengine.cpp:222
msgid "DVD-RAM"
msgstr "DVD-RAM"

#: soliddeviceengine.cpp:225
msgid "DVD+R"
msgstr "DVD+R"

#: soliddeviceengine.cpp:228
msgid "DVD+RW"
msgstr "DVD+RW"

#: soliddeviceengine.cpp:231
msgid "DVD+DL"
msgstr "DVD+DL"

#: soliddeviceengine.cpp:234
msgid "DVD+DLRW"
msgstr "DVD+DLRW"

#: soliddeviceengine.cpp:237
msgid "BD"
msgstr "BD"

#: soliddeviceengine.cpp:240
msgid "BD-R"
msgstr "BD-R"

#: soliddeviceengine.cpp:243
msgid "BD-RE"
msgstr "BD-RE"

#: soliddeviceengine.cpp:246
msgid "HDDVD"
msgstr "HDDVD"

#: soliddeviceengine.cpp:249
msgid "HDDVD-R"
msgstr "HDDVD-R"

#: soliddeviceengine.cpp:252
msgid "HDDVD-RW"
msgstr "HDDVD-RW"

#: soliddeviceengine.cpp:254
msgid "Supported Media"
msgstr "Media Didukung"

#: soliddeviceengine.cpp:256
msgid "Read Speed"
msgstr "Kecepatan Baca"

#: soliddeviceengine.cpp:257
msgid "Write Speed"
msgstr "Kecepatan Tulis"

#: soliddeviceengine.cpp:265
msgid "Write Speeds"
msgstr "Kecepatan Tulis"

#: soliddeviceengine.cpp:273
msgid "Storage Volume"
msgstr "Volume Penyimpanan"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Other"
msgstr "Lainnya"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Unused"
msgstr "Tak Digunakan"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "File System"
msgstr "Sistem File"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Partition Table"
msgstr "Tabel Partisi"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Raid"
msgstr "Raid"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Encrypted"
msgstr "Terenkripsi"

#: soliddeviceengine.cpp:279 soliddeviceengine.cpp:281
msgid "Usage"
msgstr "Penggunaan"

#: soliddeviceengine.cpp:281
#, kde-format
msgid "Unknown"
msgstr "Tak Diketahui"

#: soliddeviceengine.cpp:284
msgid "Ignored"
msgstr "Diabaikan"

#: soliddeviceengine.cpp:285
msgid "File System Type"
msgstr "Tipe Sistem File"

#: soliddeviceengine.cpp:286
msgid "Label"
msgstr "Label"

#: soliddeviceengine.cpp:287
msgid "UUID"
msgstr "UUID"

#: soliddeviceengine.cpp:296
msgid "Encrypted Container"
msgstr "Wadah Terenkripsi"

#: soliddeviceengine.cpp:308
msgid "OpticalDisc"
msgstr "Cakram Optik"

#: soliddeviceengine.cpp:314
msgid "Audio"
msgstr "Audio"

#: soliddeviceengine.cpp:317
msgid "Data"
msgstr "Data"

#: soliddeviceengine.cpp:320
msgid "Video CD"
msgstr "Video CD"

#: soliddeviceengine.cpp:323
msgid "Super Video CD"
msgstr "Super Video CD"

#: soliddeviceengine.cpp:326
msgid "Video DVD"
msgstr "Video DVD"

#: soliddeviceengine.cpp:329
msgid "Video Blu Ray"
msgstr "Video Blu Ray"

#: soliddeviceengine.cpp:331
msgid "Available Content"
msgstr "Isi Tersedia"

#: soliddeviceengine.cpp:334
msgid "Unknown Disc Type"
msgstr "Tipe Cakram Tak Diketahui"

#: soliddeviceengine.cpp:334
msgid "CD Rom"
msgstr "CD Rom"

#: soliddeviceengine.cpp:334
msgid "CD Recordable"
msgstr "CD Dapat Direkam"

#: soliddeviceengine.cpp:335
msgid "CD Rewritable"
msgstr "CD Dapat Ditulis Ulang"

#: soliddeviceengine.cpp:335
msgid "DVD Rom"
msgstr "DVD Rom"

#: soliddeviceengine.cpp:335
msgid "DVD Ram"
msgstr "DVD Ram"

#: soliddeviceengine.cpp:336
msgid "DVD Recordable"
msgstr "DVD Dapat Direkam"

#: soliddeviceengine.cpp:336
msgid "DVD Rewritable"
msgstr "DVD Dapat Ditulis Ulang"

#: soliddeviceengine.cpp:337
msgid "DVD Plus Recordable"
msgstr "DVD Plus Dapat Direkam"

#: soliddeviceengine.cpp:337
msgid "DVD Plus Rewritable"
msgstr "DVD Plus Dapat Ditulis Ulang"

#: soliddeviceengine.cpp:338
msgid "DVD Plus Recordable Duallayer"
msgstr "DVD Plus Dapat Direkam Lapisan Ganda"

#: soliddeviceengine.cpp:338
msgid "DVD Plus Rewritable Duallayer"
msgstr "DVD Plus Dapat Ditulis Ulang Lapisan Ganda"

#: soliddeviceengine.cpp:339
msgid "Blu Ray Rom"
msgstr "Blu Ray Rom"

#: soliddeviceengine.cpp:339
msgid "Blu Ray Recordable"
msgstr "Blu Ray Dapat Direkam"

#: soliddeviceengine.cpp:340
msgid "Blu Ray Rewritable"
msgstr "Blu Ray Dapat Ditulis Ulang"

#: soliddeviceengine.cpp:340
msgid "HD DVD Rom"
msgstr "HD DVD Rom"

#: soliddeviceengine.cpp:341
msgid "HD DVD Recordable"
msgstr "HD DVD Dapat Direkam"

#: soliddeviceengine.cpp:341
msgid "HD DVD Rewritable"
msgstr "HD DVD Dapat Ditulis Ulang"

#: soliddeviceengine.cpp:343
msgid "Disc Type"
msgstr "Tipe Cakram"

#: soliddeviceengine.cpp:344
msgid "Appendable"
msgstr "Dapat Ditambahkan"

#: soliddeviceengine.cpp:345
msgid "Blank"
msgstr "Kosong"

#: soliddeviceengine.cpp:346
msgid "Rewritable"
msgstr "Dapat Ditulis Ulang"

#: soliddeviceengine.cpp:347
msgid "Capacity"
msgstr "Kapasitas"

#: soliddeviceengine.cpp:355
msgid "Camera"
msgstr "Kamera"

#: soliddeviceengine.cpp:357 soliddeviceengine.cpp:371
msgid "Supported Protocols"
msgstr "Protokol Didukung"

#: soliddeviceengine.cpp:358 soliddeviceengine.cpp:372
msgid "Supported Drivers"
msgstr "Driver Didukung"

#: soliddeviceengine.cpp:369
msgid "Portable Media Player"
msgstr "Pemutar Media Portabel"

#: soliddeviceengine.cpp:383
msgid "Battery"
msgstr "Baterai"

#: soliddeviceengine.cpp:386
msgid "Unknown Battery"
msgstr "Baterai Tak Diketahui"

#: soliddeviceengine.cpp:386
msgid "PDA Battery"
msgstr "Baterai PDA"

#: soliddeviceengine.cpp:386
msgid "UPS Battery"
msgstr "Baterai UPS"

#: soliddeviceengine.cpp:387
msgid "Primary Battery"
msgstr "Baterai Utama"

#: soliddeviceengine.cpp:387
msgid "Mouse Battery"
msgstr "Baterai Mouse"

#: soliddeviceengine.cpp:388
msgid "Keyboard Battery"
msgstr "Baterai Keyboard"

#: soliddeviceengine.cpp:388
msgid "Keyboard Mouse Battery"
msgstr "Baterai Mouse Keyboard"

#: soliddeviceengine.cpp:389
msgid "Camera Battery"
msgstr "Baterai Kamera"

#: soliddeviceengine.cpp:389
msgid "Phone Battery"
msgstr "Baterai Ponsel"

#: soliddeviceengine.cpp:389
msgid "Monitor Battery"
msgstr "Pemantau Baterai"

#: soliddeviceengine.cpp:390
msgid "Gaming Input Battery"
msgstr "Baterai Input Game"

#: soliddeviceengine.cpp:390
msgid "Bluetooth Battery"
msgstr "Baterai Bluetooth"

#: soliddeviceengine.cpp:393
msgid "Not Charging"
msgstr "Tidak Mengecas"

#: soliddeviceengine.cpp:393
msgid "Charging"
msgstr "Mengecas"

#: soliddeviceengine.cpp:393
msgid "Discharging"
msgstr "Gak Ngecas"

#: soliddeviceengine.cpp:394
msgid "Fully Charged"
msgstr "Tercas Penuh"

#: soliddeviceengine.cpp:396
msgid "Plugged In"
msgstr "Ditancapkan"

#: soliddeviceengine.cpp:397
msgid "Type"
msgstr "Tipe"

#: soliddeviceengine.cpp:398
msgid "Charge Percent"
msgstr "Persen Pengecasan"

#: soliddeviceengine.cpp:399
msgid "Rechargeable"
msgstr "Dapat Dicas Ulang"

#: soliddeviceengine.cpp:400
msgid "Charge State"
msgstr "Kondisi Pengecasan"

#: soliddeviceengine.cpp:425
msgid "Type Description"
msgstr "Deskripsi Tipe"

#: soliddeviceengine.cpp:430
msgid "Device Types"
msgstr "Tipe Peranti"

#: soliddeviceengine.cpp:454 soliddeviceengine.cpp:553
msgid "Size"
msgstr "Ukuran"

#: soliddeviceengine.cpp:533
#, kde-format
msgid "Filesystem is not responding"
msgstr "Filesistem tidaklah merespons"

#: soliddeviceengine.cpp:533
#, kde-format
msgid "Filesystem mounted at '%1' is not responding"
msgstr "Filesistem yang dikaitkan di '%1' tidaklah merespons"

#: soliddeviceengine.cpp:551
msgid "Free Space"
msgstr "Ruang Kosong"

#: soliddeviceengine.cpp:552
msgid "Free Space Text"
msgstr "Teks Ruang Kosong"

#: soliddeviceengine.cpp:554
msgid "Size Text"
msgstr "Teks Ukuran"

#: soliddeviceengine.cpp:580
msgid "Temperature"
msgstr "Temperatur"

#: soliddeviceengine.cpp:581
msgid "Temperature Unit"
msgstr "Unit Temperatur"

#: soliddeviceengine.cpp:625 soliddeviceengine.cpp:629
msgid "In Use"
msgstr "Digunakan"

#~ msgid "Network Interface"
#~ msgstr "Antarmuka Jaringan"

#~ msgid "Interface Name"
#~ msgstr "Nama Antarmuka"

#~ msgid "Wireless"
#~ msgstr "Nirkabel"

#~ msgid "Hardware Address"
#~ msgstr "Alamat Peranti Keras"

#~ msgid "MAC Address"
#~ msgstr "Alamat MAC"

#~ msgid "AC Adapter"
#~ msgstr "Adaptor AC"

#~ msgid "Button"
#~ msgstr "Tombol"

#~ msgid "Lid Button"
#~ msgstr "Tombol Tutup"

#~ msgid "Power Button"
#~ msgstr "Tombol Hidupkan"

#~ msgid "Sleep Button"
#~ msgstr "Tombol Tidur"

#~ msgid "Unknown Button Type"
#~ msgstr "Tipe Tombol Tak Diketahui"

#~ msgid "Has State"
#~ msgstr "Memiliki Kondisi"

#~ msgid "State Value"
#~ msgstr "Nilai Kondisi"

#~ msgid "Pressed"
#~ msgstr "Ditekan"

#~ msgid "Audio Interface"
#~ msgstr "Antarmuka Audio"

#~ msgid "ALSA"
#~ msgstr "ALSA"

#~ msgid "Open Sound System"
#~ msgstr "Sistem Suara Terbuka"

#~ msgid "Unknown Audio Driver"
#~ msgstr "Driver Audio Tak Diketahui"

#~ msgid "Driver"
#~ msgstr "Driver"

#~ msgid "Driver Handle"
#~ msgstr "Penanganan Driver"

#~ msgid "Name"
#~ msgstr "Nama"

#~ msgid "Unknown Audio Interface Type"
#~ msgstr "Tipe Antarmuka Audio Tak Diketahui"

#~ msgid "Audio Control"
#~ msgstr "Kontrol Audio"

#~ msgid "Audio Input"
#~ msgstr "Masukan Audio"

#~ msgid "Audio Output"
#~ msgstr "Keluaran Audio"

#~ msgid "Audio Device Type"
#~ msgstr "Tipe Divais Audio"

#~ msgid "Internal Soundcard"
#~ msgstr "Kartu Suara Internal"

#~ msgid "USB Soundcard"
#~ msgstr "Kartu Suara USB"

#~ msgid "Firewire Soundcard"
#~ msgstr "Kartu Suara Firewire"

#~ msgid "Headset"
#~ msgstr "Headset"

#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgid "Soundcard Type"
#~ msgstr "Tipe Kartu Suara"

#~ msgid "DVB Interface"
#~ msgstr "Antarmuka DVB"

#~ msgid "Device Adapter"
#~ msgstr "Adaptor Divais"

#~ msgid "DVB Unknown"
#~ msgstr "DVB Tak Diketahui"

#~ msgid "DVB Audio"
#~ msgstr "Audio DVB"

#~ msgid "DVB Ca"
#~ msgstr "DVB Ca"

#~ msgid "DVB Demux"
#~ msgstr "DVB Demux"

#~ msgid "DVB DVR"
#~ msgstr "DVB DVR"

#~ msgid "DVB Frontend"
#~ msgstr "Ujung Depan DVB"

#~ msgid "DVB Net"
#~ msgstr "DVB Net"

#~ msgid "DVB OSD"
#~ msgstr "DVB OSD"

#~ msgid "DVB Sec"
#~ msgstr "DVB Sec"

#~ msgid "DVB Video"
#~ msgstr "DVB Video"

#~ msgid "DVB Device Type"
#~ msgstr "Tipe Divais DVB"

#~ msgid "Device Index"
#~ msgstr "Indeks Divais"

#~ msgid "Video"
#~ msgstr "Video"

#~ msgid "Driver Handles"
#~ msgstr "Penanganan Driver"
