# Translation of plasma_applet_org.kde.plasma.battery to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2009, 2010, 2015, 2016, 2017, 2019, 2020, 2021, 2022, 2023.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2010, 2011.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-07 00:38+0000\n"
"PO-Revision-Date: 2023-02-22 21:36+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/BatteryItem.qml:107
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/BatteryItem.qml:171
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Batterihelsa er på berre %1 %, og du bør derfor byta batteri. Ta kontakt med "
"maskinvare­produsenten."

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgid "Time To Full:"
msgstr "Tid til ferdig lada:"

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgid "Remaining Time:"
msgstr "Tid att:"

#: package/contents/ui/BatteryItem.qml:197
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "Estimerer …"

#: package/contents/ui/BatteryItem.qml:209
#, kde-format
msgid "Battery Health:"
msgstr "Batterihelse:"

#: package/contents/ui/BatteryItem.qml:215
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/BatteryItem.qml:229
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Batteriet er sett opp til berre å ladast opp til %1 %."

#: package/contents/ui/CompactRepresentation.qml:105
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:142
#, kde-format
msgid "Fully Charged"
msgstr "Ferdig ladd"

#: package/contents/ui/logic.js:28
#, kde-format
msgid "Discharging"
msgstr "Ladar ut"

#: package/contents/ui/logic.js:30
#, kde-format
msgid "Charging"
msgstr "Ladar"

#: package/contents/ui/logic.js:32
#, kde-format
msgid "Not Charging"
msgstr "Ladar ikkje"

#: package/contents/ui/logic.js:35
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Ikkje sett i"

#: package/contents/ui/main.qml:117
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "Power and Battery"
msgstr "Straumstyring"

#: package/contents/ui/main.qml:117 package/contents/ui/main.qml:286
#, kde-format
msgid "Power Management"
msgstr "Straumstyring"

#: package/contents/ui/main.qml:149
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Batterinivå: %1 % (ladar ikkje)"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Batterinivå: %1 % (kopla til, men ladar framleis ut)"

#: package/contents/ui/main.qml:153
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Batterinivå: %1 % (ladar)"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Battery at %1%"
msgstr "Batterinivå: %1 %"

#: package/contents/ui/main.qml:164
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "Straumforsyninga er ikkje kraftig nok til å lada batteriet"

#: package/contents/ui/main.qml:168
#, kde-format
msgid "No Batteries Available"
msgstr "Ingen batteri tilgjengelege"

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 til ferdig lada"

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 att"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Not charging"
msgstr "Ladar ikkje"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr "Automatisk kvilemodus og skjermlåsing er slått av"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "Eitt program har bedt om aktivering av ytingsmodus"
msgstr[1] "%1 program har bedt om aktivering av ytingsmodus"

#: package/contents/ui/main.qml:192
#, kde-format
msgid "System is in Performance mode"
msgstr "Systemet er i ytingsmodus"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "Eitt program har bedt om aktivering av straumsparemodus"
msgstr[1] "%1 program har bedt om aktivering av straumsparemodus"

#: package/contents/ui/main.qml:200
#, kde-format
msgid "System is in Power Save mode"
msgstr "Systemet er i straumsparemodus"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "Batterielementet har slått på hindring for heila systemet"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Klarte ikkje å aktivera %1-modus"

#: package/contents/ui/main.qml:311
#, kde-format
msgid "&Show Energy Information…"
msgstr "&Vis energiinformasjon …"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr "Vis batteriprosent på ikonet viss ikkje fullada"

#: package/contents/ui/main.qml:329
#, fuzzy, kde-format
#| msgid "&Configure Energy Saving…"
msgid "&Configure Power Management…"
msgstr "&Set opp straumsparing …"

# Teksten på engelsk er litt rar (korfor ha med «manually»?). Poenget er at når ein kryssar av for dette, vil ikkje maskina automatisk låsa skjermen eller gå i kvilemodus viss ein er inaktiv for lenge. Prøver ei omsetting som gjer dette klarare. Er òg meir naturleg å nemna skjermlåsing først (sidan det er det som skjer først).
#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Hindra automatisk skjermlåsing og kvilemodus"

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Maskina di er sett opp til å ikkje gå i kvile-/dvalemodus når du lukkar att "
"lokket og ein ekstern skjerm er kopla til."

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "%1 program hindrar kvilemodus og skjermlåsing:"
msgstr[1] "%1 program hindrar kvilemodus og skjermlåsing:"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 hindrar kvilemodus og skjermlåsing (%2)"

#: package/contents/ui/PowerManagementItem.qml:109
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 hindrar kvilemodus og skjermlåsing (ukjend grunn)"

#: package/contents/ui/PowerManagementItem.qml:111
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr "Eit program hindrar kvilemodus og skjermlåsing (%1)"

#: package/contents/ui/PowerManagementItem.qml:113
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr "Eit program hindrar kvilemodus og skjermlåsing (ukjend grunn)"

#: package/contents/ui/PowerManagementItem.qml:117
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:119
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: ukjend grunn"

#: package/contents/ui/PowerManagementItem.qml:121
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr "Ukjent program: %1"

#: package/contents/ui/PowerManagementItem.qml:123
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "Ukjent program: ukjend grunn"

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr "Straumsparing"

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr "Balansert"

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr "Yting"

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr "Straumprofil"

#: package/contents/ui/PowerProfileItem.qml:97
#, fuzzy, kde-format
#| msgid "No Batteries Available"
msgctxt "Power profile"
msgid "Not available"
msgstr "Ingen batteri tilgjengelege"

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Ytingsmodusen vart slått av for å redusera varme­utviklinga, då maskina har "
"funne ut at du truleg har ho plassert i fanget."

#: package/contents/ui/PowerProfileItem.qml:194
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "Utyingsmodusen er utilgjengeleg grunna overoppheting av datamaskina."

#: package/contents/ui/PowerProfileItem.qml:196
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Utyingsmodusen er utilgjengeleg."

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Ytinga kan verta redusert for å redusera varme­utviklinga, då maskina har "
"funne ut at du truleg har ho plassert i fanget."

#: package/contents/ui/PowerProfileItem.qml:211
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Ytinga kan verta redusert grunna overoppheting av datamaskina."

#: package/contents/ui/PowerProfileItem.qml:213
#, kde-format
msgid "Performance may be reduced."
msgstr "Ytinga kan verta redusert."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "Eitt program har bedt om aktivering av %2:"
msgstr[1] "%1 program har bedt om aktivering av %2:"

#: package/contents/ui/PowerProfileItem.qml:242
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:261
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""
