# translation of plasma_applet_devicenotifier.po to Esperanto
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Pierre-Marie Pédrot <pedrotpmx@wanadoo.fr>, 2007.
# Cindy McKee <cfmckee@gmail.com>, 2007, 2009.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2024-01-11 07:32+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/DeviceItem.qml:188
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 libera de %2"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Aliranta…"

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Foriganta…"

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr "Ankoraŭ ne malŝtopu! Dosieroj ankoraŭ estas en transiĝo…"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr "Malfermi en Dosiermastrumilo"

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr "Munti kaj Malfermi"

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr "Elĵeti"

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr "Sekure demeti"

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr "Munti"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr "Demeti Ĉiujn"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Klaku por demeti tiun ĉi aparaton."

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No removable devices attached"
msgstr "Ne estas demeteblaj aparatoj alligataj"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No disks available"
msgstr "Neniuj diskoj je dispono"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Most Recent Device"
msgstr "Lasta Alligata Aparato"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr "Neniuj Aparatoj je Dispono"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Agordi Demeteblajn Aparatojn…"

#: package/contents/ui/main.qml:233
#, kde-format
msgid "Removable Devices"
msgstr "Demeteblaj Aparatoj"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Non Removable Devices"
msgstr "Nedemeteblaj Aparatoj"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "All Devices"
msgstr "Ĉiuj aparatoj"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Montri ŝprucfenestron kiam nova aparato estas enŝtopata."

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr "Aparata Statuso"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr "Aparato povas nun esti sekure forigita"

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr "La aparato povas nun esti sekure forigita."

#: plugin/ksolidnotify.cpp:198
#, kde-format
msgid "You are not authorized to mount this device."
msgstr "Vi ne estas rajtigita kroĉi ĉi tiun aparaton."

#: plugin/ksolidnotify.cpp:201
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "Vi ne estas rajtigita demeti ĉi tiun aparaton."

#: plugin/ksolidnotify.cpp:204
#, kde-format
msgid "You are not authorized to eject this disc."
msgstr "Vi ne estas rajtigita elĵeti ĉi tiun diskon."

#: plugin/ksolidnotify.cpp:211
#, kde-format
msgid "Could not mount this device as it is busy."
msgstr "Ne eblis kroĉi ĉi tiun aparaton ĉar ĝi estas okupita."

#: plugin/ksolidnotify.cpp:242
#, kde-format
msgid "One or more files on this device are open within an application."
msgstr ""
"Unu aŭ pluraj dosieroj sur ĉi tiu aparato estas malfermitaj en aplikaĵo."

#: plugin/ksolidnotify.cpp:244
#, kde-format
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] ""
"Unu aŭ pluraj dosieroj sur ĉi tiu aparato estas malfermitaj en aplikaĵo "
"\"%2\"."
msgstr[1] ""
"Unu aŭ pluraj dosieroj sur ĉi tiu aparato estas malfermitaj en sekvaj "
"aplikaĵoj: %2."

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ", "

#: plugin/ksolidnotify.cpp:265
#, kde-format
msgid "Could not mount this device."
msgstr "Ne eblis kroĉi ci aparaton."

#: plugin/ksolidnotify.cpp:268
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "Ne eblis forigi tiun ĉi aparaton."

#: plugin/ksolidnotify.cpp:271
#, kde-format
msgid "Could not eject this disc."
msgstr "Ne eblis elĵeti ĉi tiun diskon."
