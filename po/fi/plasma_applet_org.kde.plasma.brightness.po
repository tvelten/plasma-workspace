# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Tommi Nieminen <translator@legisign.org>, 2023.
# SPDX-FileCopyrightText: 2024 Kimmo Kujansuu <mrkujansuu@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2024-02-07 22:40+0200\n"
"Last-Translator: Kimmo Kujansuu <mrkujansuu@gmail.com>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.01.95\n"

#: package/contents/ui/BrightnessItem.qml:71
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Brightness and Color"
msgstr "Kirkkaus ja väri"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Screen brightness at %1%"
msgstr "Näytön kirkkaus %1 %"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "Näppäimistön kirkkaus %1 %"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "Yövalo ei käytössä"

#: package/contents/ui/main.qml:107
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "Yövalo %1 K"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Säädä näytön kirkkautta vierittämällä"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "Käytä yövaloa tai poista se käytöstä keskinapsautuksella"

#: package/contents/ui/main.qml:258
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Aseta yövalo…"

#: package/contents/ui/NightColorItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "Ei käytössä"

#: package/contents/ui/NightColorItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Ei käytettävissä"

#: package/contents/ui/NightColorItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Ei käytössä"

#: package/contents/ui/NightColorItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Ei käynnissä"

#: package/contents/ui/NightColorItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Käytössä"

#: package/contents/ui/NightColorItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Aamusiirtymä"

#: package/contents/ui/NightColorItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "Päivä"

#: package/contents/ui/NightColorItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Iltasiirtymä"

#: package/contents/ui/NightColorItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Yö"

#: package/contents/ui/NightColorItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1 K"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Configure…"
msgstr "Asetukset…"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Enable and Configure…"
msgstr "Käyttöönotto ja asetukset…"

#: package/contents/ui/NightColorItem.qml:168
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Siirtymä päivään valmis:"

#: package/contents/ui/NightColorItem.qml:170
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Siirtymä yöhön ajastettu:"

#: package/contents/ui/NightColorItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Siirtymä yöhön valmis:"

#: package/contents/ui/NightColorItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Siirtymä päivään ajastettu:"

#: package/contents/ui/PopupDialog.qml:70
#, kde-format
msgid "Display Brightness"
msgstr "Näytön kirkkaus"

#: package/contents/ui/PopupDialog.qml:101
#, kde-format
msgid "Keyboard Brightness"
msgstr "Näppäimistön kirkkaus"

#: package/contents/ui/PopupDialog.qml:132
#, kde-format
msgid "Night Light"
msgstr "Yövalo"
