# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_baloosearch5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2023-10-04 12:45+0300\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Poedit 3.3.2\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "פתיחת התיקייה המכילה"

#: baloosearchrunner.cpp:82
#, kde-format
msgid "Audios"
msgstr "קטעי שמע"

#: baloosearchrunner.cpp:83
#, kde-format
msgid "Images"
msgstr "תמונות"

#: baloosearchrunner.cpp:84
#, kde-format
msgid "Videos"
msgstr "סרטונים"

#: baloosearchrunner.cpp:85
#, kde-format
msgid "Spreadsheets"
msgstr "גיליונות נתונים"

#: baloosearchrunner.cpp:86
#, kde-format
msgid "Presentations"
msgstr "מצגות"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Folders"
msgstr "תיקיות"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Documents"
msgstr "מסמכים"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Archives"
msgstr "ארכיונים"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Texts"
msgstr "טקסטים"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr "קבצים"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "חפש קבצים, הודעות מייל ואנשי קשר"
