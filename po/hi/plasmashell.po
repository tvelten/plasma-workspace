# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-17 00:39+0000\n"
"PO-Revision-Date: 2023-02-28 21:23+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: Hindi <kde-l10n-hi@kde.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "राघवेंद्र कामत"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raghu@raghukamath.com"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "माउस क्रिया प्लगइन विन्यस्त करें"

#: desktopview.cpp:211
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "प्लाज़्मा"

#: desktopview.cpp:214
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "प्लाज़्मा"

#: desktopview.cpp:217
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "प्लाज़्मा"

#: desktopview.cpp:220
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "प्लाज़्मा"

#: desktopview.cpp:223
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "प्लाज़्मा"

#: desktopview.cpp:226
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "प्लाज़्मा"

#: desktopview.cpp:252
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "प्लाज़्मा"

#: desktopview.cpp:256
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "प्लाज़्मा"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr ""

#: desktopview.cpp:263
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "प्लाज़्मा"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr ""

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "प्लाज़्मा शेल"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "क्यूएमएल जावास्क्रिप्ट डीबगर सक्षम करें"

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "दुर्घटना के बाद प्लाज़्मा-शेल को स्वचालित रूप से पुनरारंभ न करें"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "दिए गए शेल प्लगइन को लोड करने के लिए बाध्य करें"

#: main.cpp:110
#, kde-format
msgid "Replace an existing instance"
msgstr "एक वर्तमान चलित अनुप्रयोग को प्रतिस्थापित करें"

#: main.cpp:113
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"परीक्षण मोड को सक्षम बनाता है और परीक्षण वातावरण स्थापित करने के लिए जावास्क्रिप्ट "
"फ़ाइल खाका निर्दिष्ट करता है।"

#: main.cpp:114
#, kde-format
msgid "file"
msgstr "फ़ाइल"

#: main.cpp:118
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "उपयोक्ता प्रतिक्रिया के लिए उपलब्ध विकल्पों की सूची दिखाता है"

#: main.cpp:200
#, kde-format
msgid "Plasma Failed To Start"
msgstr "प्लाज़्मा चालू होने में विफल हुआ"

#: main.cpp:201
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"प्लाज़्मा प्रारंभ होने में असमर्थ है क्योंकि यह ओपनजीएल २ या सॉफ़्टवेयर विकल्प का सही उपयोग "
"नहीं कर सका।\n"
"कृपया जांचें कि आपके ग्राफिक ड्राइवर सही तरीके से स्थापित हैं।"

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "ध्वनी बंद है"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "माइक्रोफोन बंद है"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 मूक है"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "स्पर्श-पैड चालू है"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "स्पर्श-पैड बंद है"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "वाइफाइ चालू है"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "वाइफाइ बंद है"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "ब्लूटूथ चालू है"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "ब्लूटूथ बंद है"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "मोबाइल इन्टरनेट चालू है"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "मोबाइल इन्टरनेट बंद है"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "स्क्रीन पर कुंजीपटल सक्रिय"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "स्क्रीन पर कुंजीपटल निष्क्रिय"

#: panelview.cpp:1013
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Panel Configuration"
msgstr ""

#: panelview.cpp:1015
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Panel Configuration"
msgstr ""

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr "%1 पर आंतरिक स्क्रीन"

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr "%3 पर %1 %2"

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr "वियोजित स्क्रीन %1"

#: shellcorona.cpp:197 shellcorona.cpp:199
#, kde-format
msgid "Show Desktop"
msgstr "डेस्कटॉप दिखाएँ"

#: shellcorona.cpp:199
#, kde-format
msgid "Hide Desktop"
msgstr "डेस्कटॉप चिपाएँ"

#: shellcorona.cpp:215
#, kde-format
msgid "Show Activity Switcher"
msgstr "गतिविधि परिवर्तक दिखाएं"

#: shellcorona.cpp:226
#, kde-format
msgid "Stop Current Activity"
msgstr "वर्तमान गतिविधि बंद करें"

#: shellcorona.cpp:234
#, kde-format
msgid "Switch to Previous Activity"
msgstr "पिछली गतिविधि पर जाएँ"

#: shellcorona.cpp:242
#, kde-format
msgid "Switch to Next Activity"
msgstr "अगले गतिविधि पर जाएँ"

#: shellcorona.cpp:257
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "कार्य प्रबंधक प्रविष्टि %1 सक्रिय करें"

#: shellcorona.cpp:284
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "डेस्कटॉप और पैनल प्रबंधित करें..."

#: shellcorona.cpp:306
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "पैनल के बीच कुंजीपटल फ़ोकस ले जाएँ"

#: shellcorona.cpp:2051
#, kde-format
msgid "Add Panel"
msgstr "पैनल जोडें"

#: shellcorona.cpp:2093
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "खाली %1"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "सॉफ्टवेयर रेंडरर जो उपयोग में है"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "सॉफ्टवेयर रेंडरर जो उपयोग में है"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "रेंडरिंग अवक्रमित हो सकता है"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "फिर कभी न दिखाएँ"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "पैनल की गिनती"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "पैनलों को गिनेगा"

#~ msgid ""
#~ "Load plasmashell as a standalone application, needs the shell-plugin "
#~ "option to be specified"
#~ msgstr ""
#~ "प्लाज़्मा शेल को एक स्वतंत्र अनुप्रयोग के रूप में लोड करें, शेल-प्लगइन विकल्प निर्दिष्ट करने के "
#~ "की आवश्यकता है"

#~ msgid "Unable to load script file: %1"
#~ msgstr "स्क्रिप्ट फ़ाइललोड करने में अक्षम : %1"
