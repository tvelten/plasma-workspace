# translation of plasma_applet_battery.po to Khmer
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2008, 2009, 2010, 2012.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-07 00:38+0000\n"
"PO-Revision-Date: 2012-07-04 09:13+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Language: km-CM\n"

#: package/contents/ui/BatteryItem.qml:107
#, fuzzy, kde-format
#| msgctxt "overlay on the battery, needs to be really tiny"
#| msgid "%1%"
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:171
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgid "Time To Full:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgid "Remaining Time:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:197
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr ""

#: package/contents/ui/BatteryItem.qml:209
#, fuzzy, kde-format
#| msgid "Battery:"
msgid "Battery Health:"
msgstr "ថ្ម ៖"

#: package/contents/ui/BatteryItem.qml:215
#, fuzzy, kde-format
#| msgctxt "overlay on the battery, needs to be really tiny"
#| msgid "%1%"
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:229
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:105
#, fuzzy, kde-format
#| msgctxt "overlay on the battery, needs to be really tiny"
#| msgid "%1%"
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:142
#, kde-format
msgid "Fully Charged"
msgstr ""

#: package/contents/ui/logic.js:28
#, fuzzy, kde-format
#| msgid "%1% (discharging)"
msgid "Discharging"
msgstr "%1% (មិន​កំពុង​បញ្ចូល)"

#: package/contents/ui/logic.js:30
#, fuzzy, kde-format
#| msgid "%1% (charging)"
msgid "Charging"
msgstr "%1% (កំពុង​បញ្ចូល)"

#: package/contents/ui/logic.js:32
#, fuzzy, kde-format
#| msgid "%1% (charging)"
msgid "Not Charging"
msgstr "%1% (កំពុង​បញ្ចូល)"

#: package/contents/ui/logic.js:35
#, fuzzy, kde-format
#| msgctxt "Battery is not plugged in"
#| msgid "Not present"
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "មិនបង្ហាញ"

#: package/contents/ui/main.qml:117
#, fuzzy, kde-format
#| msgctxt "Label for power management inhibition"
#| msgid "Power management enabled:"
msgid "Power and Battery"
msgstr "បាន​បើក​កា​រគ្រប់គ្រង​ថាមពល ៖"

#: package/contents/ui/main.qml:117 package/contents/ui/main.qml:286
#, fuzzy, kde-format
#| msgctxt "Label for power management inhibition"
#| msgid "Power management enabled:"
msgid "Power Management"
msgstr "បាន​បើក​កា​រគ្រប់គ្រង​ថាមពល ៖"

#: package/contents/ui/main.qml:149
#, fuzzy, kde-format
#| msgid "%1% (charging)"
msgid "Battery at %1%, not Charging"
msgstr "%1% (កំពុង​បញ្ចូល)"

#: package/contents/ui/main.qml:151
#, fuzzy, kde-format
#| msgid "%1% (charging)"
msgid "Battery at %1%, plugged in but still discharging"
msgstr "%1% (កំពុង​បញ្ចូល)"

#: package/contents/ui/main.qml:153
#, fuzzy, kde-format
#| msgid "%1% (charging)"
msgid "Battery at %1%, Charging"
msgstr "%1% (កំពុង​បញ្ចូល)"

#: package/contents/ui/main.qml:156
#, fuzzy, kde-format
#| msgid "Battery:"
msgid "Battery at %1%"
msgstr "ថ្ម ៖"

#: package/contents/ui/main.qml:164
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr ""

#: package/contents/ui/main.qml:168
#, kde-format
msgid "No Batteries Available"
msgstr ""

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr ""

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr ""

#: package/contents/ui/main.qml:179
#, fuzzy, kde-format
#| msgid "%1% (charging)"
msgid "Not charging"
msgstr "%1% (កំពុង​បញ្ចូល)"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr ""

#: package/contents/ui/main.qml:188
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] ""

#: package/contents/ui/main.qml:192
#, kde-format
msgid "System is in Performance mode"
msgstr ""

#: package/contents/ui/main.qml:196
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] ""

#: package/contents/ui/main.qml:200
#, kde-format
msgid "System is in Power Save mode"
msgstr ""

#: package/contents/ui/main.qml:260
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "អាប់ភ្លេត​ថ្ម​បាន​បើកការ​ហាមឃាត់​ទូទាំង​ប្រព័ន្ធ​"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Failed to activate %1 mode"
msgstr ""

#: package/contents/ui/main.qml:311
#, fuzzy, kde-format
#| msgid "Show charge &information"
msgid "&Show Energy Information…"
msgstr "បង្ហាញ​ព័ត៌មាន​ពេល​បញ្ចូល​ថ្ម"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr ""

#: package/contents/ui/main.qml:329
#, fuzzy, kde-format
#| msgctxt "Label for power management inhibition"
#| msgid "Power management enabled:"
msgid "&Configure Power Management…"
msgstr "បាន​បើក​កា​រគ្រប់គ្រង​ថាមពល ៖"

#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] ""

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:109
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:111
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:113
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:117
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:119
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:121
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:123
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:97
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:194
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:196
#, fuzzy, kde-format
#| msgctxt "Label for power management inhibition"
#| msgid "Power management enabled:"
msgid "Performance mode is unavailable."
msgstr "បាន​បើក​កា​រគ្រប់គ្រង​ថាមពល ៖"

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:211
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:213
#, fuzzy, kde-format
#| msgctxt "Label for power management inhibition"
#| msgid "Power management enabled:"
msgid "Performance may be reduced."
msgstr "បាន​បើក​កា​រគ្រប់គ្រង​ថាមពល ៖"

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] ""

#: package/contents/ui/PowerProfileItem.qml:242
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:261
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""

#, fuzzy
#~| msgctxt "overlay on the battery, needs to be really tiny"
#~| msgid "%1%"
#~ msgctxt "Placeholder is brightness percentage"
#~ msgid "%1%"
#~ msgstr "%1%"

#, fuzzy
#~| msgid "Screen Brightness:"
#~ msgid "Battery and Brightness"
#~ msgstr "ពន្លឺ​អេក្រង់ ៖"

#, fuzzy
#~| msgid "Screen Brightness:"
#~ msgid "Brightness"
#~ msgstr "ពន្លឺ​អេក្រង់ ៖"

#, fuzzy
#~| msgid "Battery:"
#~ msgid "Battery"
#~ msgstr "ថ្ម ៖"

#, fuzzy
#~| msgid "Screen Brightness:"
#~ msgid "Display Brightness"
#~ msgstr "ពន្លឺ​អេក្រង់ ៖"

#, fuzzy
#~| msgid "Screen Brightness:"
#~ msgid "Keyboard Brightness"
#~ msgstr "ពន្លឺ​អេក្រង់ ៖"

#, fuzzy
#~| msgctxt "Label for power management inhibition"
#~| msgid "Power management enabled:"
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "បាន​បើក​កា​រគ្រប់គ្រង​ថាមពល ៖"

#, fuzzy
#~| msgid "%1% (charging)"
#~ msgid "%1% Charging"
#~ msgstr "%1% (កំពុង​បញ្ចូល)"

#, fuzzy
#~| msgctxt "tooltip"
#~| msgid "Plugged in"
#~ msgid "%1% Plugged in"
#~ msgstr "បាន​ដោត"

#, fuzzy
#~| msgctxt "overlay on the battery, needs to be really tiny"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery's capacity degradation"
#~ msgid "%1%"
#~ msgstr "%1%"

#, fuzzy
#~| msgctxt "overlay on the battery, needs to be really tiny"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery capacity"
#~ msgid "%1%"
#~ msgstr "%1%"

#, fuzzy
#~| msgctxt "overlay on the battery, needs to be really tiny"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery name"
#~ msgid "%1:"
#~ msgstr "%1%"

#, fuzzy
#~| msgid "1 hour"
#~| msgid_plural "%1 hours"
#~ msgid "1 hour "
#~ msgid_plural "%1 hours "
#~ msgstr[0] "%1 ម៉ោង"

#~ msgid "1 minute"
#~ msgid_plural "%1 minutes"
#~ msgstr[0] "%1 នាទី"

#, fuzzy
#~| msgid "AC Adapter:"
#~ msgid "AC Adapter"
#~ msgstr "អាដាប់ទ័រ AC ៖​"

#, fuzzy
#~| msgid "Plugged in"
#~ msgid "Plugged In"
#~ msgstr "បាន​ដោត"

#, fuzzy
#~| msgid "Not plugged in"
#~ msgid "Not Plugged In"
#~ msgstr "មិន​បានដោត​"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until full: %1"
#~ msgstr "ពេលវេលា​នៅ​សល់ ៖"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until empty: %1"
#~ msgstr "ពេលវេលា​នៅ​សល់ ៖"

#, fuzzy
#~| msgid "%1% (charged)"
#~ msgid "%1% (charged)"
#~ msgstr "%1% (បានបញ្ចូល​ថាមពល)"

#, fuzzy
#~| msgid "AC Adapter:"
#~ msgctxt "tooltip"
#~ msgid "AC Adapter:"
#~ msgstr "អាដាប់ទ័រ AC ៖​"

#, fuzzy
#~| msgid "Plugged in"
#~ msgctxt "tooltip"
#~ msgid "<b>Plugged in</b>"
#~ msgstr "បាន​ដោត"

#, fuzzy
#~| msgid "Not plugged in"
#~ msgctxt "tooltip"
#~ msgid "<b>Not plugged in</b>"
#~ msgstr "មិន​បានដោត​"

#~ msgctxt "overlay on the battery, needs to be really tiny"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Configure Battery Monitor"
#~ msgstr "កំណត់​រចនាសម្ព័ន្ធ​កម្មវិធី​ត្រួតពិនិត្យ​ថ្ម"

#~ msgctxt "Suspend the computer to RAM; translation should be short"
#~ msgid "Sleep"
#~ msgstr "ដេក"

#~ msgctxt "Suspend the computer to disk; translation should be short"
#~ msgid "Hibernate"
#~ msgstr "ក្រាំង"

#~ msgid "Show the state for &each battery present"
#~ msgstr "បង្ហាញ​ស្ថានភាព​សម្រាប់​វត្តមាន​ថ្ម​នីមួយៗ"

#~ msgid "<b>Battery:</b>"
#~ msgstr "<b>ថ្ម ៖</b>"

#~ msgctxt "tooltip"
#~ msgid "<b>AC Adapter:</b>"
#~ msgstr "<b>អាដាប់ទ័រ AC ៖</b>"

#~ msgctxt "tooltip"
#~ msgid "Not plugged in"
#~ msgstr "មិន​បានដោត​"
